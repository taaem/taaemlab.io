var gulp = require('gulp');
var git = require('gulp-git');
var sass = require('gulp-sass');
var jade = require('gulp-jade');

gulp.task('jade',function() {
  gulp.src('./app/views/index.jade')
    .pipe(jade())
    .pipe(gulp.dest('./public/'))
})
gulp.task('sass', function () {
  gulp.src('./app/sass/main.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./public/'));
});
gulp.task('assets', function () {
  gulp.src('./app/assets/img/*')
    .pipe(gulp.dest('./public/'))
})
gulp.task('js', function () {
  gulp.src('./app/js/*')
    .pipe(gulp.dest('./public/'))
})
gulp.task('build',['jade', 'sass', 'assets', 'js'], function () {

})
gulp.task('default',['build'], function() {
  gulp.watch(['./app/js/**/*.js'],['js'])
  gulp.watch(['./app/assets/**/*'],['assets'])
  gulp.watch(['./app/sass/**/*.scss'],['sass'])
  gulp.watch(['./app/views/**/*.jade'],['jade'])
});
